# workbench

Parametric design of a wood workbench using CadQuery.

![shot](screenshot.png)

![isometric export to svg](iso.png)


alto base = 89 cm, con tapa la altura será 91cm

16 tornillos de 8 o 9 cm, con tuercas y rondanas
1 perno de 8 o 9 cm de mayor calibre para la articulación del brazo.
1 espiga de unos 30 o 35 cm. Cuatro tuercas, cuatro rondanas.
8 tirafondos de 11 cm
